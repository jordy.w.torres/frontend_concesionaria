import '../css/stylea.css';
import Header from "./Header";
import PresentarTablaAuto from './PresentarTablaAuto';
import { Marcas } from '../hooks/Conexion';
import { borrarSesion, getToken } from '../utilidades/Sessionutil';
import { obtenerAutos } from '../hooks/Conexion';
import { useNavigate } from 'react-router-dom';
import Mensajes from '../utilidades/Mensajes';
import { useState } from 'react';
import {MarcasCantidad,AutosCantidad } from '../hooks/Conexion';
const Inicio = () => {
    const navegation = useNavigate();
    const [nro, setNro] = useState(0);
    const [nroA, setNroA] = useState(0);

   /* Marcas(getToken()).then((info) => {
        if (info.error == true && info.messaje == 'Acceso denegado. Token a expirado') {
            borrarSesion();
            Mensajes(info.mensajes);
            navegation("/sesion")
        } else {
            setNro(info.data);
        }
    });*/
    //const autos = Autos(getToken());
    MarcasCantidad(getToken()).then((info) => {
        if (info.error == true && info.messaje == 'Acceso denegado. Token a expirado') {
  
            Mensajes(info.mensajes);
            navegation("/sesion")
        } else {
            setNroA(info.info);
            
        }
    });
    AutosCantidad(getToken()).then((info) => {
        if (info.error == true && info.messaje == 'Acceso denegado. Token a expirado') {
            Mensajes(info.mensajes);
            navegation("/sesion")
        } else {
            setNro(info.info);
            
        }
    });
    const autos = obtenerAutos();
    return (
        <div>  
         <div className="wrapper"style={{backgroundColor:"",margin:"50px"}}>
            <div className="d-flex flex-column">
                <div className="content">
                    <Header />
                    {/**DE aqui cuerpo */}
                    <div className='container-fluid'>
                        <div className="row">

                            <div className="col-xl-3 col-md-6 mb-4">
                                <div className="card border-left-primary shadow h-100 py-2">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                    Todas las marcas</div>
                                                <div className="h5 mb-0 font-weight-bold text-gray-800">{nroA}</div>
                                            </div>
                                            <div className="col-auto">
                                                <i className="fas fa-calendar fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-xl-3 col-md-6 mb-4" >
                                <div className="card border-left-success shadow h-100 py-2" >
                                    <div className="card-body" >
                                        <div className="row no-gutters align-items-center">
                                            <div className="col mr-2" >
                                                <div className="text-xs font-weight-bold text-success text-uppercase mb-1">
                                                    Todos los autos</div>
                                                <div className="h5 mb-0 font-weight-bold text-gray-800">{nro}</div>
                                            </div>
                                            <div className="col-auto">
                                                <i className="fas fa-dollar-sign fa-2x text-gray-300"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
       
        </div>
        </div>
 
    );
}

export default Inicio;