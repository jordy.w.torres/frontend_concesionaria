import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import ModificarAuto from './ModificarAuto';
import { listarInformacion } from '../hooks/Conexion';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';

function Factura() {
  const [datos, setDatos] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [autoSeleccionado, setAutoSeleccionado] = useState(null);

  useEffect(() => {
    obtenerFacturas();
  }, []);

  const obtenerFacturas = async () => {
    try {
      const data = await listarInformacion();
      console.log(data.info);
      setDatos(data.info);
    } catch (error) {
      console.log(error);
    }
  };

  const handleVerTodo = (factura) => {
    setAutoSeleccionado(factura);
    setShowModal(true);
  };

  const closeModal = () => {
    setShowModal(false);
  };

  return (
    <div className="card">
      <div className="card-header bg-black"></div>
      <div className="card-body">
        <div className="container">
          <div className="row">
            <div className="col-xl-12">
              <i className="far fa-building text-danger fa-6x float-start"></i>
            </div>
          </div>
          <div className="row">
            <div className="col-xl-12">
              <ul className="list-unstyled float-end">
                <li style={{ fontSize: '30px', color: 'red' }}>Facturas</li>
              </ul>
            </div>
          </div>
          <div className="row text-center">
            <h3 className="text-uppercase text-center mt-3" style={{ fontSize: '40px' }}>
              Lista
            </h3>
            <p>123456789</p>
          </div>
          <div className="row mx-3">
            <table className="table">
              <thead>
                <tr>
                  <th scope="col">Numero Factura</th>
                  <th scope="col">Cedula</th>
                  <th scope="col">Auto</th>
                  <th scope="col">Placa</th>
                  <th scope="col"></th>
                </tr>
              </thead>
              <tbody>
                {datos.map((factura, index) => (
                  <tr key={index}>
                    <td>{factura.numerofactura}</td>
                    <td>{factura.persona.identificacion}</td>
                    <td>{factura.auto.modelo}</td>
                    <td>{factura.auto.placa}</td>
                    <td>
                      <button className="btn btn-primary" onClick={() => handleVerTodo(factura)}>
                        Ver todo
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div className="card-footer bg-black"></div>
      <Modal show={showModal} onHide={closeModal}>
        <Modal.Header closeButton>
          <Modal.Title>Información del Auto</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          {autoSeleccionado && (
            <>
              <p>Número de factura: {autoSeleccionado.numerofactura}</p>
              <p>fecha emitida: {autoSeleccionado.fecha_emision}</p>
              <p>Repuesto usado: {autoSeleccionado.repuesto.nombre}</p>
              <p>Cédula: {autoSeleccionado.persona.identificacion}</p>
              <p>Auto: {autoSeleccionado.auto.modelo}</p>
              <p>Placa: {autoSeleccionado.auto.placa}</p>
              <p>total: {autoSeleccionado.total}</p>
              {/* Agrega más detalles de la factura si es necesario */}
            </>
          )}
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={closeModal}>
            Cerrar
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
}

export default Factura;
