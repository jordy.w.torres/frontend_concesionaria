import React, { useState, useEffect } from 'react';
import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import InputGroup from 'react-bootstrap/InputGroup';
import { useNavigate } from 'react-router-dom';
import Row from 'react-bootstrap/Row';
import { useForm } from 'react-hook-form';
import {guardarReparacion, obtenerRepuestos, obtenerModeloAutos } from '../hooks/Conexion';
import { getToken } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
function FacturaReparacion() {
  const navegation = useNavigate();
  const { register, handleSubmit, formState: { errors } } = useForm();
  const [validated, setValidated] = useState(false);
  const [repuestos, setRepuestos] = useState([]);
  const [auto, setAuto] = useState([]);
  useEffect(() => { 
    obtenerListaRepuestos();
    obtenerListaAutos();
  }, []);

  const obtenerListaRepuestos = async () => {
    try {
      const repuestos = await obtenerRepuestos();
      setRepuestos(repuestos);
    } catch (error) {
      console.error(error);
      // Manejar el error de alguna manera adecuada en tu aplicación
    }
  };
  const obtenerListaAutos = async () => {
    try {
      const auto = await obtenerModeloAutos();
      setAuto(auto);
    } catch (error) {
      console.error(error);
      // Manejar el error de alguna manera adecuada en tu aplicación
    }
  };
  const onSubmit = (data) => {
    console.log(data);

    var datos = {
      "idRepuesto": data.repuesto,
      "idAuto":data.auto,
      "fechaEmision": data.fecha,
      "direccion": data.direccion,
      "cantidad": data.Cantidadrepuestos,
      "identificacion": data.cedula
    };
    /*"fecha_emision": new Date(),
      "direccion": data.direccion,
      "subTtotal": data.subtotal,
      "iva": data.iva,
      "total": data.total,
      "cantidad": data.cantidad*/
    guardarReparacion(datos, getToken()).then((info) => {
      console.log(info);
      if (info.code != 200) {
        //console.log(info);
        mensajes(info.msg, 'error', 'Exitoso');
        //msgError(info.message);            
      } else {
        mensajes(info.msg);
        navegation('/reparacion');
      }
    }
    );
  };

  return (
    <form className="user" onSubmit={handleSubmit(onSubmit)}>
      <div className="form-group">
        <input type="date" {...register('fecha', { required: true })} className="form-control form-control-user" placeholder="Ingrese la fecha de emision" />
        {errors.fecha && errors.fecha.type === 'required' && <div className='alert alert-danger'>Ingrese la fecha de emision</div>}
      </div>
      <div className="form-group">
        <input type="text" className="form-control form-control-user" placeholder="Ingrese el direccion" {...register('direccion', { required: true })} />
        {errors.direccion && errors.direccion.type === 'required' && <div className='alert alert-danger'>Ingrese la direccion</div>}
      </div>
      <div className="form-group">
  <input type="text" className="form-control form-control-user" placeholder="Ingrese la cantidad de repuestos" {...register('Cantidadrepuestos', { required: true, min: 0 })} />
  {errors.Cantidadrepuestos && errors.Cantidadrepuestos.type === 'min' && <div className='alert alert-danger'>La cantidad de repuestos no puede ser negativa</div>}
  {errors.Cantidadrepuestos && errors.Cantidadrepuestos.type === 'required' && <div className='alert alert-danger'>Ingrese la cantidad de repuestos</div>}
     
</div>


<div className="form-group">
  <input type="text" className="form-control form-control-user" placeholder="Ingrese la cédula" {...register('cedula', { required: true, pattern: /^[0-9]{10}$/ })} />
  {errors.cedula && errors.cedula.type === 'required' && <div className='alert alert-danger'>Ingrese la cédula</div>}
  {errors.cedula && errors.cedula.type === 'pattern' && <div className='alert alert-danger'>La cédula debe tener 10 dígitos numéricos</div>}
</div>

      <div className="form-group">
        <select className='form-control' {...register('repuesto', { required: true })}>
          <option>Elija un repuesto</option>
          {repuestos.map((m, id) => {
            return (<option key={id} value={m.external_id}>
              {m.nombre}
            </option>)
          })}
        </select>
        {errors.repuesto && errors.repuesto.type === 'required' && <div className='alert alert-danger'>Selecione la marca</div>}
      </div>
      <div className="form-group">
        <select className='form-control' {...register('auto', { required: true })}>
          <option>Elija el auto</option>
          {auto.map((m, id) => {
            return (<option key={id} value={m.external_id}>
              {m.modelo}
            </option>)
          })}
        </select>
        {errors.auto && errors.auto.type === 'required' && <div className='alert alert-danger'>Selecione un auto</div>}

      </div>

      <hr />
      {' '}
      <Button className="btn btn-success mt-4" type="submit" >Facturar</Button>
    </form>

  );
}

export default FacturaReparacion;
