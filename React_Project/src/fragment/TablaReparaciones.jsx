import React, { useEffect, useState } from 'react';
import DataTable from 'react-data-table-component';
import { obtenerAutos, modificarAutos} from '../hooks/Conexion'; // Importa el método modificarAuto
import { borrarSesion } from '../utilidades/Sessionutil';
import mensajes from '../utilidades/Mensajes';
import { useNavigate } from 'react-router-dom';

import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import FacturaReparacion from './FacturaReparacion';

const TablaReparaciones = () => {
  const ExpandedComponent = ({ data }) => <pre>{JSON.stringify(data, null, 2)}</pre>;
  const navegation = useNavigate();
  const [data, setData] = useState([]);
  const [showModal, setShowModal] = useState(false); // Agrega el estado showModal
  const [autoSeleccionado, setAutoSeleccionado] = useState(null); // Agrega el estado autoSeleccionado
  const [mensaje, setMensaje] = useState(""); // Agrega el estado mensaje

  useEffect(() => {
    obtenerAutos()
      .then((info) => {
        if (info.error === true && info.mensaje === 'Acceso denegado. Token a expirado') {
          borrarSesion();
          mensajes(info.mensaje);
          navegation("/sesion");
        } else {
          setData(info);
          console.log(info);
        }
      })
      .catch((error) => {
        console.error(error);
        // Manejar el error de alguna manera adecuada en tu aplicación
      });
  }, []);

  const handleModificar = (row) => {
    setShowModal(true); // Mostrar el modal al hacer clic en Modificar
    setAutoSeleccionado(row); // Guardar el auto seleccionado en el estado autoSeleccionado
  };

  const handleCloseModal = () => {
    setShowModal(false); // Cerrar el modal
    setAutoSeleccionado(null); // Limpiar el auto seleccionado
  };
  const handleModificarAuto = (datosAuto) => {
    // Verificar si hay un auto seleccionado
    if (autoSeleccionado) {
      const { id } = autoSeleccionado;
      const key = localStorage.getItem('token'); // Obtener la clave API/token guardado en la cuenta
  
      console.log('Datos del auto a modificar:', datosAuto); // Mostrar por consola los datos del auto a modificar
  
      modificarAutos(key, id, datosAuto)
        .then((resultado) => {
          console.log(resultado);
          handleCloseModal(); // Cerrar el modal después de modificar el auto
        })
        .catch((error) => {
          console.error(error);
          // Manejar el error de alguna manera adecuada en tu aplicación
        });
    }
  };
  

const columns = [
  {
    name: 'Modelo',
    selector: (row) => row.modelo,
  },
    {
      name: 'Placa',
      selector: (row) => row.placa,
    },
    {
      name: 'Color',
      selector: (row) => row.color,
    },
    {
      name: 'Kilometraje',
      selector: (row) => `${row.kilometraje}`,
    },
    {
      name: 'Número de Puertas',
      selector: (row) => `${row.numeroPuertas}`,
    },
    {
      name: 'Tipo de Combustible',
      selector: (row) => `${row.tipoCombustible}`,
    },
    {
      name: 'Cedula',
      selector: (row) => `${row.dni_duenio}`,
    },
  {
    name: 'Acciones',
    cell: (row) => (
      <button className="btn btn-primary" style={{fontSize:'14px'}} onClick={() => handleModificar(row)}>
        Facturar
      </button>
    ),
  },
];
  return (
    <>
      <DataTable
        columns={columns}
        data={data}
        expandableRows
        expandableRowsComponent={ExpandedComponent}
      />

      <div className="model_box">
        <Modal
          show={showModal}
          onHide={handleCloseModal}
          backdrop="static"
          keyboard={false}
        >
          <Modal.Header closeButton>
            <Modal.Title>Facturar Reparacion</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <FacturaReparacion onModificarAuto={handleModificarAuto} />

            
          </Modal.Body>
          <Modal.Footer>
            {mensaje && <p>{mensaje}</p>} {/* Mostrar el mensaje si existe */}
            <Button variant="secondary" onClick={handleCloseModal}>
              Cerrar
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    </>
  );
};

export default TablaReparaciones;

